# Photography Policy

Photography is a common part of GNOME events. These events are an important opportunity for GNOME to get images for marketing and fundraising purposes, and people therefore may be taking photographs at our request or encouragement. There may also be press photographers present.

We recognize that some attendees might not want to have their picture taken, or that it might make them feel uncomfortable. If this applies to you, event organizers will do their best to ensure that your wishes are complied with.

## Guidelines for attendees

If you don't want to have your picture taken, please make this known to event organizers before or near the beginning of the event. During some events, you will be issued a special badge to indicate that your picture should not be taken. There may be a photo-free zone where you can sit during talks - feel free to ask event organizers about this.

Photographers should ask your permission either before or after taking your picture. If this doesn't happen, you should feel free to ask them to stop or to delete any pictures they have taken. If you don't feel comfortable doing this, just ask an event organizer and they will assist you.

There are some cases where all attendees should expect to have their picture taken. This includes if you participate in a group photograph, or if you give a talk.

## Guidelines for photographers

If you are taking photographs at a GNOME event, make yourself available to those you are taking pictures of. Ensure that you get permission from your subjects either before or after you have taken their picture. Permission from parents or guardians should be requested for all minors. If someone asks you not to take their picture, don't. If someone asks you to delete or unpublish a picture you have taken of them, politely comply.

Don't harass people by repeatedly taking their picture without permission. Don’t take pictures of those wearing photo-free badges. If a photo-free zone has been set up, do not include this area in your photographs.

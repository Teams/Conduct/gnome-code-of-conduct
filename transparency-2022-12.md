# Transparency report through December 2022 - GNOME Code of Conduct Committee

GNOME's Code of Conduct is our standard of behavior for participants in GNOME.  This is the Code of Conduct Committee's periodic report of activities.

Note: the last report was on September 2020.  Not much has happened, and we missed the deadlines for the periodic reports!  We'll do better in the future.

## Reports

There was a report about a heated conversation on Matrix.  The report was non-actionable, but we reminded the people in question to be more constructive.

There was a report about harassing comments made by a non-Foundation member on Matrix. The reported person has not been seen again, and thus we could not enact a ban.

There was a report about a non-Foundation member in a non-Foundation venue, that we deemed non-actionable.

There was a report about a blog syndicated on planet.gnome.org; we talked to the reported person about the effect words have on people.

A report from Reddit was handled by the /r/gnome moderator.  We also asked for the GNOME CoC to be linked in a more prominent place in the subreddit's page.

We have enacted a ban on a person who was posting to discourse.gnome.org in contravention of the Code of Conduct.

## Changes to GNOME's Code of Conduct

Clarified the "Safety versus Comfort" section.

Included language suitable for Events, not just online spaces.  Added social events.

As a reminder, the CoC's sources are published at https://gitlab.gnome.org/Teams/Conduct/gnome-code-of-conduct - you can also see the history of changes there.

## Changes in the Code of Conduct Committee

Felipe Borges has decided to leave the committe after participating since 2019.  We thank 
Felipe for his work, and for being a great team member!

The current members of the CoC Committee are:

* Federico Mena Quintero (chairperson and liaison to the Board)
* Rosanna Yuen
* Thibault Martin

## Other activities

Members of the CoC Committee were available to be contacted during all virtual events.

Members of the CoC Committee attended GUADEC 2022 along with trained volunteer 
incident responders.

We sponsored a training session for incident response with Otter Tech in November 2020.  
In total there are now 16 people from GNOME who have taken such training.  Thanks for 
making GNOME a safer space!

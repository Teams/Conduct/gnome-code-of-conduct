# Transparency report for January 2024 - GNOME Code of Conduct Committee

GNOME's Code of Conduct is our standard of behavior for participants in GNOME. This is the Code of Conduct Committee's report of activities for 2023.

## Changes in the Code of Conduct Committee

There have been two added CoC members during 2023, Anisa Kuci and Michael Downey.

The current members of the CoC Committee are:

* Federico Mena Quintero (chairperson)
* Rosanna Yuen
* Anisa Kuci
* Michael Downey (liaison to the Board)

All the members of the CoC committee have attended Code of Conduct Incident Response Workshop by Otter Tech and are trained to handle incident report in the GNOME community events.

## Reports

Since January 2023, the committee has gotten a total of 9 reports, by 6 different people.

(#39 #50): We received a report about a member who had used an inappropriate and offensive language in some gaming channels where other members were participating as well.

The committee reviewed all the proof and came to the conclusion of keeping an eye on this member’s action in the future and stay alert if anything similar happens again. The committee didn't take further action since the reported behavior happened in channels not connected with the foundation or community specifically.

(#42 #41): We received a request from a member who had a temporary ban from the project to regain their privileges since the ban period was terminated.

(#52 #53 #54):
We received reports about a member who used disrespectful language in the online discussion platform towards other members. After reviewing the discussion thread the committee decided to contact the reported person and ask them to moderate their language and be more careful. The situation calmed down quickly, nevertheless we provided further support for the reporter in case similar behavior will be repeated again.

(#83):
We received a report by a member which was not within the scope of CoC or even a proper report rather than request for feedback on an academic research. The CoC committee didn’t act on this issue.

(#84):
Another issue was opened which was not a report that is within the CoC scope. The committee didn’t act on this issue.


## Changes related to the CoC processes

The committee proposed a new process of reporting incidents, which keeps the anonymity of the reporter through the website https://conduct.gnome.org/ - reports submitted here go to our internal GitLab project for tracking reports.

There is also a generic email alias to be used for reports: conduct@gnome.org also creates a report in our internal GitLab project.

## Talks and meetings of the CoC committee

The CoC committee has monthly meetings for general updates, and they organize ad-hoc meetings when they receive reports.  Also, they have organized in-person meetings during some of the GNOME events.

In July 2023 the committee met at GUADEC in Riga, Latvia (3 members in person and 1 member online) and in GNOME.Asia in Kathmandu, Nepal. 

## Easier ways to contact the CoC committee

We now have easier ways to contact the committee:

* https://conduct.gnome.org - contains the GNOME Code of Conduct and a form to submit an incident report

* conduct@gnome.org - so you can submit incident reports, questions, etc.

Both methods create a report in the internal GitLab project that the CoC committee uses to track incidents.

Thanks to the sysadmin team for their quick response on these.
